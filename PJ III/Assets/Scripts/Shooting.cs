using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public float bulletForce = 20f;
    public AudioSource audioSource;
    public AudioClip shootingSound;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    public void Shoot()
    {
        // Calculate the direction from the fire point to the mouse position
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = 10f; // Set the distance from the camera
        Vector3 targetPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        Vector3 direction = targetPosition - firePoint.position;

        // Instantiate the bullet and set its rotation to face the direction
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);
        bullet.transform.right = direction.normalized;

        // Add force to the bullet in the direction it's facing
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(bullet.transform.right * bulletForce, ForceMode2D.Impulse);

        // Play the shooting sound effect
        audioSource.PlayOneShot(shootingSound);
    }
}
