using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectibles : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        KeyGrabber keyGrabber = other.GetComponent<KeyGrabber>();

        if (keyGrabber != null)
        {
            keyGrabber.GrabbedKey();
            gameObject.SetActive(false);
        }
    }
}
