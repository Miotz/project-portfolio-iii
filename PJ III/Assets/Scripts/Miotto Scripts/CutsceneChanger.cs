using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CutsceneChanger : MonoBehaviour
{
    public float secondsToWait;
    private void WaitFadeOut()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void Start()
    {
        LeanTween.delayedCall(secondsToWait, WaitFadeOut);
    }
}
