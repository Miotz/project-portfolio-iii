using UnityEngine;
using System.Collections;

public class ArrowFire : MonoBehaviour
{
    public GameObject projectile;
    public GameObject targetObject;
    public float fireRate = 1.0f;

    private float lastFireTime = 0.0f;

    void Update()
    {
        if (Time.time - lastFireTime >= 1 / fireRate)
        {
            Vector3 direction = targetObject.transform.position - transform.position;
            GameObject proj = Instantiate(projectile, transform.position, Quaternion.identity);
            proj.GetComponent<Rigidbody2D>().velocity = direction.normalized * 10;
            lastFireTime = Time.time;
        }
    }
}