using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemyFollowEndless : MonoBehaviour
{
    

    private float speed = 3.3f;
    private Transform player;
    public float lineOfSight;
    public float shootingRange;
    public GameObject enemyBullet;
    public GameObject enemyBulletParent;
    private float fireRate = 5f;
    private float nextFireTime;
    private int health = 20;
    public float bulletSpeed;
    private AudioSource source;
    public AudioClip enemyShootingSound;
    public AudioClip enemyDeathSound;
    private Score score;


    void Start()
    {
        source = GetComponent<AudioSource>();
        score = FindObjectOfType<Score>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }


    void Update()
    {
        float distanceFromPlayer = Vector2.Distance(player.position, transform.position);

        if (distanceFromPlayer < lineOfSight && distanceFromPlayer > shootingRange)
        {
            transform.position = Vector2.MoveTowards(this.transform.position, player.position, speed * Time.deltaTime);
        }
        else if (distanceFromPlayer <= shootingRange && nextFireTime < Time.time)
        {
            GameObject bulletInstance = Instantiate(enemyBullet, enemyBulletParent.transform.position, Quaternion.identity);
            Vector2 bulletDirection = (player.position - enemyBulletParent.transform.position).normalized;
            Rigidbody2D bulletRb = bulletInstance.GetComponent<Rigidbody2D>();
            bulletRb.velocity = bulletDirection * bulletSpeed;
            nextFireTime = Time.time + fireRate;
            source.PlayOneShot(enemyShootingSound);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSight);
        Gizmos.DrawWireSphere(transform.position, shootingRange);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            TakeDamage();
        }
    }
    private void TakeDamage()
    {
        health -= FindObjectOfType<BpmManager>().damage;

        if (health <= 0)
        {
            score.scoreCount += 50;
            source.PlayOneShot(enemyDeathSound);
            Destroy(gameObject);
        }
    }
}
