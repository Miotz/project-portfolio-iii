using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToMap1 : MonoBehaviour
{
    private bool onZone;
    public Animator anim;
    public float secondsToWait;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            onZone = true;
            Debug.Log("Sono nella zona");
        }

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K) && onZone)
        {
            anim.SetTrigger("FadeOut");
            LeanTween.delayedCall(secondsToWait, ChangeLevel);
        }
    }

    private void ChangeLevel()
    {
        SceneManager.LoadScene("Mappa1");
    }
}

