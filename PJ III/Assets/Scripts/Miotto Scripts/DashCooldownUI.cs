using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashCooldownUI : MonoBehaviour
{
    public Slider dashSlider;
    private float dashMaximumCD = 3f;
    private float dashCurrentCD;
    private bool dashReady = false;
    

   
     public void Update()
        {
           doDashStaff();
        }

    private void doDashStaff()
    {
        dashSlider.value = dashCurrentCD / dashMaximumCD;

        dashCurrentCD += Time.deltaTime;
        dashCurrentCD = Mathf.Clamp(dashCurrentCD, 0.0f, dashMaximumCD);


        if (dashCurrentCD == dashMaximumCD)
        {
            dashReady = true;
            dashSlider.gameObject.SetActive(false);

        }

        if (dashCurrentCD < dashMaximumCD)
        {
            dashReady = false;
            dashSlider.gameObject.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && dashReady == true)
        {
            dashCurrentCD = 0f;
            dashReady = false;
            Debug.Log("lo scatto � pronto");
        }
    }
}
