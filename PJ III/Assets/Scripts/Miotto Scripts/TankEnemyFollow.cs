using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TankEnemyFollow : MonoBehaviour
{
    private float speed = 2.5f;
    private Transform player;
    public float lineOfSight;
    public float attackRange;
    private float attackDamage = 6f;
    private float attackDelay = 0.5f;
    private float attackDuration = 0.5f;
    private float nextAttackTime;
    private bool isAttacking;
    private int health = 150;
    private Rigidbody2D rb;
    public GameObject KeyModel;
    
    private int counter;

    void Start()
    {
        
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        counter = 0;
    }

    void Update()
    {
        float distanceFromPlayer = Vector2.Distance(player.position, transform.position);

        if (distanceFromPlayer < lineOfSight && distanceFromPlayer > attackRange)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
        else if (distanceFromPlayer <= attackRange && Time.time > nextAttackTime && !isAttacking)
        {
            StartCoroutine(AttackCoroutine());
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSight);
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }

    IEnumerator AttackCoroutine()
    {
        isAttacking = true;
        rb.velocity = Vector2.zero;
        nextAttackTime = Time.time + attackDelay;
        float endTime = Time.time + attackDuration;

        while (Time.time < endTime)
        {
            if (Vector2.Distance(player.position, transform.position) <= attackRange)
            {
                player.GetComponent<PlayerHealth>().TakeDamage(attackDamage);
            }
            yield return null;
        }

        isAttacking = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            TakeDamage();
        }
    }

    private void TakeDamage()
    {
        health -= FindObjectOfType<BpmManager>().damage;
        if (health <= 0)
        {
            counter++;
            if (counter == 1)
            {
                KeyDrop();
            }   
            Destroy(gameObject);            
        }
    }

    public void KeyDrop()
    {
        
        Vector3 position = transform.position;
        GameObject Key = Instantiate(KeyModel, position, Quaternion.identity);
    }
}