using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnDoorCollision : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            FindObjectOfType<KeyManager>().onDoor = true;
        }
        else
        {
            FindObjectOfType<KeyManager>().onDoor = false;
        }
    }

}
