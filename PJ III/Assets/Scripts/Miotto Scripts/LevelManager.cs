using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    private float counter;
    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    void Start()
    {
        counter = 0;
    }

    
    void Update()
    {
        
        if(FindObjectOfType<PlayerHealth>().isDead == true)
        {
            counter += Time.deltaTime;
            if (counter >= 2)
            {
                RestartLevel();
            }
        }
    }
}
