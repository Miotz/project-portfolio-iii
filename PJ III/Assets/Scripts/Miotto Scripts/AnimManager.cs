using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimManager : MonoBehaviour
{
    public Animator anim;

    void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 objPos = transform.position;
        Vector2 dir = mousePos - objPos;

        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        if (angle >= -22.5f && angle <= 22.5f)
        {
            //anim.SetTrigger("FacingEast");
            anim.Play("Ding_East");
        }
        else if (angle > 22.5f && angle <= 67.5f)
        {
            //anim.SetTrigger("FacingNorthEast");
            anim.Play("Ding_North_East");
        }
        else if (angle > 67.5f && angle <= 112.5f)
        {
            //anim.SetTrigger("FacingNorth");
            anim.Play("Ding_North");
        }
        else if (angle > 112.5f && angle <= 157.5f)
        {
            //anim.SetTrigger("FacingNorthWest");
            anim.Play("Ding_North_West");
        }
        else if (angle > 157.5f || angle < -157.5f)
        {
            //anim.SetTrigger("FacingWest");
            anim.Play("Ding_West");
        }
        else if (angle >= -157.5f && angle < -112.5f)
        {
            //anim.SetTrigger("FacingSouthWest");
            anim.Play("Ding_South_West");
        }
        else if (angle >= -112.5f && angle < -67.5f)
        {
            //anim.SetTrigger("FacingSouth");
            anim.Play("Ding_South");
        }
        else if (angle >= -67.5f && angle < -22.5f)
        {
            //anim.SetTrigger("FacingSouthEast");
            anim.Play("Ding_South_East");
        }
    }
}