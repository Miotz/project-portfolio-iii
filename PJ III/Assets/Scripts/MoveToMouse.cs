using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToMouse : MonoBehaviour
{
    
    
    private Vector2 mouseTarget;
    void Start()
    {
        mouseTarget = transform.position;
    }

    
    void Update()
    {
            mouseTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.up = mouseTarget - new Vector2(transform.position.x, transform.position.y);       
    }
}
