using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KeyUI : MonoBehaviour
{
    private TextMeshProUGUI keyText;


    void Start()
    {
        keyText = GetComponent<TextMeshProUGUI>();
    }

    public void UpdateKeyText(KeyGrabber keyGrabber)
    {
        keyText.text = keyGrabber.NumberOfKeys.ToString();
    }
}
