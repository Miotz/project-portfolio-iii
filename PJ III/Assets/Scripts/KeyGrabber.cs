using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyGrabber : MonoBehaviour
{
    public int NumberOfKeys { get; private set; }

    public UnityEvent<KeyGrabber> OnGrabbedKey;

    public void GrabbedKey()
    {
        NumberOfKeys++;
        OnGrabbedKey.Invoke(this);
    }
}
