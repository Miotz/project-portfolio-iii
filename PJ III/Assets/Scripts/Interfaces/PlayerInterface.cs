using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PlayerInterface
{

    
    Transform firePoint { get; set; }
    GameObject bulletPrefab { get; set; }
    float bulletForce { get; set; }

    void Shoot();
}
