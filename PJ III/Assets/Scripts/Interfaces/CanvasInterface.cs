using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface CanvasInterface 
{
    
    public Vector2 wpRect { get; set; }
}
