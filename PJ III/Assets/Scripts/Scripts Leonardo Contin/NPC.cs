using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class NPC : MonoBehaviour
{
    private bool trigger = false;
    private Collider2D other;

    public Transform ChatBackGround;
    public Transform NPCCharacter;

    private DialogueSystem dialogueSystem;

    public string Name;

    [TextArea(5, 10)]
    public string[] sentences;

    void Start()
    {
        dialogueSystem = FindObjectOfType<DialogueSystem>();
        this.gameObject.GetComponent<NPC>().enabled = false;
    }

    void Update()
    {
        // Sborra
        //TBA Inserire codice sotto come funzione invece che chiamarlo nell'update 
        if (trigger == true) 
        {
            Vector3 Pos = Camera.main.WorldToScreenPoint(NPCCharacter.position);
            Pos.y += 100;
            ChatBackGround.position = Pos;
            //Debug.Log("OnTriggerStay");
            //this.gameObject.GetComponent<NPC>().enabled = true;
            //FindObjectOfType<DialogueSystem>().EnterRangeOfNPC();
            if ((other.gameObject.tag == "Player") && Input.GetKeyDown(KeyCode.F))
            {
                //Debug.Log("IfPlayer");
                this.gameObject.GetComponent<NPC>().enabled = true;
                dialogueSystem.Names = Name;
                dialogueSystem.dialogueLines = sentences;
                FindObjectOfType<DialogueSystem>().NPCName();
            }
        }
    }

        public void OnTriggerEnter2D(Collider2D other_parameter)
        {
        if (other_parameter.tag == "Player")
            {
                this.gameObject.GetComponent<NPC>().enabled = true;
                FindObjectOfType<DialogueSystem>().EnterRangeOfNPC();
                trigger = true;
                other = other_parameter;
            }
        }

    public void OnTriggerExit2D()
    {
        FindObjectOfType<DialogueSystem>().OutOfRange();
        this.gameObject.GetComponent<NPC>().enabled = false;
        trigger = false;
    }
}