using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BpmManager : MonoBehaviour
{

    public Shooting shooting;

    public EnemyChase enemy;

    public int damage = 1;

    public float bpm = 120;
    private float secPerBeat;
    private float nextEventTime;
    private bool canClick = false;
    private bool isAnimating = false;

    private int comboMeter = 0;

    public Animator bpmAssistantAnim;
    public Animator noteValidator;

    public AudioSource audioSource;
    public AudioClip[] audioClips;

    private int randomSound;
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private List<int> playedSounds = new List<int>();
    [SerializeField] private int cont;

    public Slider comboMeterSlider;

    private float timeSinceLastEvent;
    private float error;

    private void Start()
    {
        secPerBeat = 60f / bpm;
        nextEventTime = Time.time + secPerBeat;
        cont = 0;
    }

    /*private void AnimationManager()
    {
        
            isAnimating = true;
            noteCheckTrue.Play("NoteFadingOff");
        
    }*/

    private void Update()
    {

        DoAllBpmStuff();
        
    }

    private IEnumerator SetCanClickWithDelay()
    {
        yield return new WaitForSeconds(0.1f);
        canClick = true;
    }

    private void PlaySoundEvent()
    {
        //metodo suoni casuali
         if (playedSounds.Count >= audioClips.Length)
         {
             playedSounds.Clear();
         }

         randomSound = Random.Range(0, audioClips.Length);
         while (playedSounds.Contains(randomSound))
         {
             randomSound = Random.Range(0, audioClips.Length);
         }

         playedSounds.Add(randomSound);
         audioClip = audioClips[randomSound];
         audioSource.PlayOneShot(audioClip);


        //metodo suoni consecutivi
        /*
        if (playedSounds.Count >= audioClips.Length)
        {
            playedSounds.Clear();
        }

        cont = 0;
        while (playedSounds.Contains(cont))
        {
            cont++;
        }
        if (cont > 3)
        {
            cont = 0;
        }
      
        playedSounds.Add(cont);
        audioClip = audioClips[cont];
        audioSource.PlayOneShot(audioClip);
        */
    }

    private void SetBpmVariables()
    {
        comboMeterSlider.value = comboMeter;
        StartCoroutine(SetCanClickWithDelay());
        nextEventTime += secPerBeat;
        PlaySoundEvent();
    }
    private void SetOtherVariables()
    {
        isAnimating = false;
        timeSinceLastEvent = Time.time - (nextEventTime - secPerBeat);
        error = Mathf.Abs(timeSinceLastEvent - secPerBeat / 2f);
    }

    private void SetWinVariables()
    {
        Debug.Log("HaiAzzeccato, e questo � il tuo danno: " + damage);
        //LeanTween.delayedCall(0.20f, AnimationManager);
        noteValidator.Play("NoteFading");
        comboMeter++;
        switch (comboMeter)
        {
            case 1:
                damage++;
                break;
            case 2:
                shooting.bulletForce += 2;
                break;
            case 3:
                damage += 2;
                break;
            case 4:
                damage++;
                shooting.bulletForce += 2;
                break;
            case 5:
                damage += 2;
                shooting.bulletForce += 4;
                break;
        }
        isAnimating = false;
        canClick = false;
    }

    private void SetLooseVariables()
    {
        damage = 1;
        shooting.bulletForce = 20f;
        comboMeter = 0;
        Debug.Log("errore verooooo");
    }

    private void DoAllBpmStuff()
    {
        if (Time.time >= nextEventTime)
        {
            SetBpmVariables();
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (canClick)
            {
                SetOtherVariables();

                if (error < secPerBeat / 3f)
                {
                    SetWinVariables();
                }
                else
                {
                    SetLooseVariables();
                }
            }
            else
            {
                SetLooseVariables();
            }
        }
    }

   
}
