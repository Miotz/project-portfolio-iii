using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoNextScene : MonoBehaviour
{
    
    public Animator anim;
    public string sceneName;

    private void WaitFadeOut()
    {
        SceneManager.LoadScene(sceneName);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            anim.SetTrigger("FadeOut");
            LeanTween.delayedCall(0.99f, WaitFadeOut);
        }
    }
   
}
