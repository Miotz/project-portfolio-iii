using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDash : MonoBehaviour
{

    private const float MOVE_SPEED = 5f;

    private enum State
    {
        Normal
    }

    [SerializeField] private LayerMask dashLayerMask;

    private Rigidbody2D rigidbody2D;
    private Vector3 moveDir;
    private Vector3 lastMoveDir;
    private bool isDashButtonDown;
    private State state;
    private float cooldownTimer = 3f;

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        state = State.Normal;
    }

    private void Update()
    {
        if (cooldownTimer > 0)
        {
            cooldownTimer -= Time.deltaTime;
        }

        float moveX = Input.GetAxis("Horizontal");
        float moveY = Input.GetAxis("Vertical");

        moveDir = new Vector3(moveX, moveY).normalized;
        if (moveX != 0 || moveY != 0)
        {
            // Not idle
            lastMoveDir = moveDir;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (cooldownTimer <= 0)
            {
                isDashButtonDown = true;
                cooldownTimer = 3f;
            }
        }
    }

    private void FixedUpdate()
    {
        rigidbody2D.velocity = moveDir * MOVE_SPEED;

        if (isDashButtonDown)
        {
            float dashAmount = 2f;
            Vector3 dashPosition = transform.position + lastMoveDir * dashAmount;

            RaycastHit2D raycastHit2d = Physics2D.Raycast(transform.position, lastMoveDir, dashAmount, dashLayerMask);
            if (raycastHit2d.collider != null)
            {
                dashPosition = raycastHit2d.point;
            }

            rigidbody2D.MovePosition(dashPosition);
            isDashButtonDown = false;
        }
    }
}
