using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 10f;
    public float dashForce;
    public float dashDuration;
    public float dashCoolDown = 3f;

    private Rigidbody2D rigidbody2D;
    [HideInInspector] public float dashTime;
    [HideInInspector] public float lastDashTime;
    private bool dashing;
    private bool isMoving;
    private float lastSoundTime;

    private AudioSource source;
    public AudioClip dashSound;

    [HideInInspector] public Vector2 movement;

    public TrailRenderer dashAnimation;

    
    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }
    private void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
       
        if(Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("Hai premuto solo A");
            if (Input.GetKeyDown(KeyCode.S))
            {
                Debug.Log("Hai premuto A e S");
            }
            
        }
        if (Time.time >= lastDashTime + dashCoolDown && Input.GetKeyDown(KeyCode.LeftShift))
        {
            dashTime = Time.time + dashDuration;
            lastDashTime = Time.time;
            dashing = true;
            source.PlayOneShot(dashSound);
        }

        if (Time.time <= dashTime)
        {
            dashAnimation.emitting = true;
            rigidbody2D.AddForce(movement * dashForce, ForceMode2D.Impulse);
        }
        else
        {
            rigidbody2D.velocity = movement * speed;
            dashing = false;
            dashAnimation.emitting = false;
        }
     
    }
    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        movement = new Vector2(horizontalInput, verticalInput);

        if (movement.magnitude > 0)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

        if (isMoving && Time.time >= lastSoundTime + 0.3f)
        {
           
            lastSoundTime = Time.time;
        }

    }

   
}
