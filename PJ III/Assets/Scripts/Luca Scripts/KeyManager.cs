using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class KeyManager : MonoBehaviour
{
    public int nextLevelCondition;
    public bool onDoor;
    void Start()
    {
        nextLevelCondition = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K) && nextLevelCondition >= 2 && onDoor)
        {
            SceneManager.LoadScene("Mappa1");
        }
    }
}
