using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{

    public Text scoreText;
    public int scoreCount;
    // Start is called before the first frame update
    private void Awake()
    {
        scoreCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Points: "+scoreCount.ToString();
    }
}
