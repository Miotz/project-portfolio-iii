using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class PlayerHealth : MonoBehaviour
{
    public static event Action OnPlayerDamaged;
    public static event Action OnPlayerDeath;
    public float timeCounter;
    public float health, maxHealth;
    public bool isDead;
    public Animator anim;
    private Rigidbody2D rb;
    public AudioSource audioSource;
    public AudioClip damageClip;


    private void Start()
    {
        isDead = false;
        health = maxHealth;

        anim = gameObject.GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();

        
        audioSource = GetComponent<AudioSource>();
    }

    public void die()
    {

        rb.bodyType = RigidbodyType2D.Static;
        FindObjectOfType<PlayerController>().speed = 0;
        isDead = true;
        anim.SetTrigger("isDead");

    }
    public void TakeDamage(float amount)
    {
        health -= amount;
        OnPlayerDamaged?.Invoke();

        
        audioSource.PlayOneShot(damageClip);

        if (health <= 0)
        {
            die();
            health = 0;
            Debug.Log("you're dead");
            OnPlayerDeath?.Invoke();
        }
    }
}