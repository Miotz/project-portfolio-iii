using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
  
    public void PlayGame() 
    {
      SceneManager.LoadScene ("FinalScene");
    }

    public void GoToSettings()
    {
      SceneManager.LoadScene("SettingsMenu");
    }

    public void GoToMainMenu()
    {
      SceneManager.LoadScene( "MainMenu");
    }
    
    public void QuitGame()
    {
       Application.Quit();
    }

}
