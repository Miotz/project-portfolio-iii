using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelPopUp : MonoBehaviour
{
    public GameObject nextLevelPopUpText;
       
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        { 
            nextLevelPopUpText.SetActive(true);
        }
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            nextLevelPopUpText.SetActive(false);
        }
    }
}
