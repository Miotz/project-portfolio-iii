using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public int attackDamage = 1;
    public float attackRate = 1f;
    public float attackRange = 1.5f;

    private float nextAttackTime;
    private Transform player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (Time.time >= nextAttackTime)
        {
            if (Vector3.Distance(transform.position, player.position) <= attackRange)
            {
                Attack();
            }
        }
    }

    void Attack()
    {
        nextAttackTime = Time.time + 1f / attackRate;

        player.GetComponent<PlayerHealth>().TakeDamage(attackDamage);
    }
}


