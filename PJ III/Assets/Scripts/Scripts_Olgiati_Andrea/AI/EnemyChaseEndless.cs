using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaseEndless : MonoBehaviour
{
    private Transform player;
    private float chaseSpeed = 3f;
    private float lineOfSight = 70f;
    private float attackTriggerDistance = 1.5f;
    private int health = 30;
    private int counter;
   


    private Vector2 startPosition;
    private Score score;

    void Start()
    {
        startPosition = transform.position;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        score = FindObjectOfType<Score>();
        counter = 0;
    }
    void Update()
    {
        float distanceToPlayer = Vector2.Distance(transform.position, player.position);
        
       
        if (distanceToPlayer < lineOfSight)
        {
            ChasePlayer();
        }
        else
        {
            ReturnToStart();
        }

    }
    void ChasePlayer()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.position, chaseSpeed * Time.deltaTime);
    }
    void ReturnToStart()
    {
        transform.position = Vector2.MoveTowards(transform.position, startPosition, chaseSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            TakeDamage();           
        }
    }
  

    private void TakeDamage()
    {
        health -= FindObjectOfType<BpmManager>().damage;

        if (health <= 0)
        {
            counter++;
            if (counter == 1)
            {
                score.scoreCount += 25;
            }
            Destroy(gameObject);
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSight);
        
    }
}