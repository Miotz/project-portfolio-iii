using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    [SerializeField] List<Transform> enemySpawner;
    [SerializeField] GameObject enemy, tankEnemy, rangedEnemy;

    public int counter { get; set; }

    public void Awake()
    {
        InvokeRepeating("SpawnEnemies", 2f, 15f);
    }

    public void SpawnEnemies()
    {
        int rng = Random.Range(2, 10);

        for (int i = 0; i < rng; i++)
        {
            int rndEnemySpawner = Random.Range(0, enemySpawner.Count);
            int randomEnemy = Random.Range(1, 101);
            Debug.Log("randomenemy = " + randomEnemy);
            if (randomEnemy >= 1 && randomEnemy <= 60)
            {
                Instantiate(enemy, enemySpawner[rndEnemySpawner].position, enemySpawner[rndEnemySpawner].rotation);
            }
            else if(randomEnemy >= 61 || randomEnemy <= 94)
            {
                Instantiate(rangedEnemy, enemySpawner[rndEnemySpawner].position, enemySpawner[rndEnemySpawner].rotation);
            }
            else
            {
                Debug.Log("gigante");
                Instantiate(tankEnemy, enemySpawner[rndEnemySpawner].position, enemySpawner[rndEnemySpawner].rotation);
            }
        }
    }
}
