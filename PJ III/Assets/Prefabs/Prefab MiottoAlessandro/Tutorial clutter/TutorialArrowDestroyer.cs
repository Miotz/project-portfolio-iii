using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialArrowDestroyer : MonoBehaviour
{
    public PlayerHealth ph;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Arrow"))
        {
            ph.TakeDamage(1);
            Destroy(collision.gameObject);
            Debug.Log("Arrow destroyed");
        }
    }
}
